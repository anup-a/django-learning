# :school: :mortar_board: Learning management

`django-koalalms-learning` is a Django application part of the Koala LMS project.

Status on `master`:
[![pipeline status](https://gitlab.com/koala-lms/django-learning/badges/master/pipeline.svg)](https://gitlab.com/koala-lms/django-learning/commits/master)
[![coverage report](https://gitlab.com/koala-lms/django-learning/badges/master/coverage.svg)](https://gitlab.com/koala-lms/django-learning/commits/master)

Status on `develop`:
[![pipeline status](https://gitlab.com/koala-lms/django-learning/badges/develop/pipeline.svg)](https://gitlab.com/koala-lms/django-learning/commits/develop)
[![coverage report](https://gitlab.com/koala-lms/django-learning/badges/develop/coverage.svg)](https://gitlab.com/koala-lms/django-learning/commits/develop)

## :book: Documentation

Full documentation is available on the dedicated website [“django-learning documentation”](https://koala-lms.gitlab.io/django-learning).
Otherwise, you can build it by yourself, it is build using `Sphinx`. It is located in the `docs` directory. Install the requirements from `docs/requirements.txt` file and run `make html` for instance.

## :open_hands: Contributing

If you wish to contribute to the project, you are invited to read the [Contribution Guide](CONTRIBUTING.md).

## :page_facing_up: Licence

`django-koalalms-learning` is available under the [GPLv3 (aka General Public Licence v3)](LICENSE). Below is a short extract of the licence:

    Copyright (C) 2019 Guillaume Bernard <guillaume.bernard@koala-lms.org>
        
    Koala LMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## :scroll: Credits

The logo we use as a thumbnail for this Gitlab project is a work made by Boca Tutor, called “Closed Book Icon” and available under [Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/deed). It is available on [Wikimedia Commons](https://en.wikipedia.org/wiki/File:Closed_Book_Icon.svg).
