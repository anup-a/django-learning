# Changelog for django-koalalms-learning

## v1.2b1 − 2020.03.08

This is the first beta of version 1.2. It is the result of a 6 month work, including **115 commits** by 5 contributors. Those contributors are students from the University of La Rochelle. Version 1.2b1 includes the following:

This version includes many fixes:
* `django-koalalms-learning` tracks only Django LTS 2.2.
* Course, Activities and Resource block are now lighter, with less colors.
* An issue caused files not to be deleted if clicking the “Delete” button on a resource.
* Activites and Resources now have collaborators too.
* Remove mentions to “you” in texts.
* Multiple other fixes and enhancements.

## External contributions

Students from the University of La Rochelle, as part of their year projects, joined the Koala LMS project at the beginning of october. At the moment, their contributions include:

* a preview for an **agora** on courses, in which users can ask questions, reply and votes for their favorite answers. (Arthur Baribaud, Simon Billet, Loris Le Bris, Alexis Delabarre, Célian Roland)
* a review of the dropdown menus to split “Learning” and “Teaching” on large screens (Raphaël Penault)
* a global review of breadcrumbs (Raphaël Penault)


## v1.1 - 2019.08.08

This first minor version is **NOT COMPATIBLE** with the previous one. We hope this is the very last time this kind of problem happens. This is due to a change in the registration of students on a Course we could not solve before due to a lack of time. This will not be a problem anyway because we’ve suppose there is not instance running yet.

This new releases include the following changes:

* `learning` depends on `accounts`
* Course queries are manager through a Manager
* Registration of student store more attributes: self registration, registration locked, etc.

And many style and performance improvements.

## v1.0 - 2019.06.25

Some tests were added, as well as style refactoring tasks.

## v1.0rc1 − 2019.06.04

This first release candidate version of `django-koalalms-learning` has been published the 4th of June 2019. It is the first usable version of this project. A final release is waited by the end of the month. Major features include:

* Create, view, update and delete **Courses**, **Activities** and **Resources**
* Show similar instances for each model (Course, Activity and Resource)
* Link/unlink existing activities to courses
* Link/unlink existing resources to activities
* Upload a file and attach it to a resource
* Register on courses, so become a student on that course
* Add users as collaborators on **Courses**, thus giving them some rights (CRUD rights).
* Navigate over activities and educational resources on courses.

