.. _demo-server:

Demonstration server
====================

We provide a *demo server* in the ``learning/demo`` directory. It makes it easier to test the application without loosing time to configure Django.

1. Create a Python virtual environment: ``python3 -m venv ./venv``.
2. Activate virtual environment: ``source ./venv/bin/activate``.
3. Install the application requirements: ``pip install -r requirements.txt``.
4. Go to the directory: ``cd learning/demo``.
5. Create the database and apply migrations: ``./manage.py migrate``.
6. Create a super user: ``./manage.py createsuperuser --username root --email root@localhost``
7. Start the server: ``./manage.py runserver 5000``.

.. note::
    You may want to switch from **master** branch, which contains the latest stable code, to the **develop** branch, that tracks the current developments. To do so, after **step 4**, switch, using ``git checkout develop``.

Or, all in one:

.. code-block:: bash

    git clone https://gitlab.com/koala-lms/django-learning.git
    cd django-learning
    python3 -m venv ./venv
    source ./venv/bin/activate
    pip install -r requirements.txt
    cd learning/demo
    ./manage.py migrate
    ./manage.py createsuperuser --username root --email root@localhost
    ./manage.py runserver 5000


Now, go to https://localhost:5000/admin and log in using the credentials provided for root user.
