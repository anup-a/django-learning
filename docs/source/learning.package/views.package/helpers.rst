Views helpers
-------------

.. automodule:: learning.views.helpers
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:
