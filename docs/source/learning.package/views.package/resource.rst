.. _resource_views:

Resource views
--------------

.. automodule:: learning.views.resource
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:
