.. _course_views:

Course views
------------

.. automodule:: learning.views.course
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:
